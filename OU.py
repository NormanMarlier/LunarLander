import random
import numpy as np


class OU(object):

    @staticmethod
    def function(x, mu, theta, sigma):
        return theta * (mu - x) + sigma * np.random.randn(1)

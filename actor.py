import tensorflow as tf
import keras.backend as K

from keras.models import Sequential, Model
from keras.layers import Dense, Flatten, Input, concatenate, Lambda


HIDDEN1_UNITS = 60
HIDDEN2_UNITS = 120


class Actor(object):
    """
    This class implement

    """

    def __init__(self, sess, state_size, action_size, batch_size, tau, learning_rate):
        self.sess = sess
        self.BATCH_SIZE = batch_size
        self.TAU = tau
        self.LEARNING_RATE = learning_rate

        K.set_session(sess)

        # Now create the model
        self.model, self.weights, self.state = self.create_actor_network(state_size)
        self.target_model, self.target_weights, self.target_state = self.create_actor_network(state_size)
        self.action_gradient = tf.placeholder(tf.float32, [None, action_size])
        self.params_grad = tf.gradients(self.model.output, self.weights, -self.action_gradient)
        grads = zip(self.params_grad, self.weights)
        self.optimize = tf.train.AdamOptimizer(learning_rate).apply_gradients(grads)
        self.sess.run(tf.global_variables_initializer())

    def train(self, states, action_grads):
        self.sess.run(self.optimize, feed_dict={self.state: states, self.action_gradient: action_grads})

    def target_train(self):
        """
        Target network - approximations are more stable
        :return:
        """
        actor_weights = self.model.get_weights()
        actor_target_weights = self.target_model.get_weights()
        for i in range(len(actor_weights)):
            actor_target_weights[i] = self.TAU * actor_weights[i] + (1 - self.TAU) * actor_target_weights[i]
        self.target_model.set_weights(actor_target_weights)

    @staticmethod
    def create_actor_network(state_size):
        s = Input(shape=[state_size])
        h0 = Dense(HIDDEN1_UNITS, activation='relu')(s)
        h1 = Dense(HIDDEN2_UNITS, activation='relu')(h0)
        main_engine = Dense(1, activation='tanh')(h1)
        left_right_engine = Dense(1, activation='tanh')(h1)
        v = concatenate([main_engine, left_right_engine], axis=-1)
        model = Model(input=s, output=v)
        return model, model.trainable_weights, s

"""
This script runs the RL algorithm
"""
import numpy as np
import gym
import logging
import tensorflow as tf
import json
import argparse


from actor import Actor
from critic import Critic
from replay_buffer import ReplayBuffer
from gym import wrappers
from matplotlib import pyplot as plt

from OU import OU

OU = OU()


# Logging config
FORMAT = '%(asctime)s :: %(levelname)s :: %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)

# Define the constant
BUFFER_SIZE = 100000
BATCH_SIZE = 128
GAMMA = 0.99
EPSILON = 0.8
TAU = 0.001  # Target Network HyperParameters
LRA = 0.0001  # Learning rate for Actor
LRC = 0.001  # Learning rate for Critic


def train_game():
    """

    :return: /
    """

    action_dim = 2  # Main Engine / Left-Right engine
    state_dim = 8  # Position, velocity, angular position, angular velocity, ground contact (2x)

    episode_count = 100
    max_steps = 2000
    reward = []
    action_mean = np.zeros((episode_count, max_steps))
    action_std = np.zeros((episode_count, max_steps))

    np.random.seed(1337)

    # Start the session of Tensorflow
    sess = tf.Session()
    from keras import backend as K
    K.set_session(sess)

    # Declare Actor/Critic/Replay Buffer
    actor = Actor(sess, state_dim, action_dim, BATCH_SIZE, TAU, LRA)
    critic = Critic(sess, state_dim, action_dim, BATCH_SIZE, TAU, LRC)
    buff = ReplayBuffer(BUFFER_SIZE)  # Create replay buffer

    # Import gym environment
    env = gym.make('LunarLanderContinuous-v2')

    # Start episode
    for i in range(episode_count):
        logging.info('Episode : %s', i)
        logging.info('Replay Buffer : %s', buff.count())

        s_t = env.reset()

        done = 0
        total_reward = 0

        # Time steps
        for j in range(max_steps):

            loss = 0

            #  Choose an action
            a_t = np.zeros(action_dim)

            logging.debug('shape of s_t : %s', s_t.shape)

            a_t = actor.model.predict(s_t.reshape(1, s_t.shape[0])).reshape(2)

            # Add noise with OU process
            n_tt = np.random.normal(0, 0.3)
            n_tr = np.random.normal(0, 0.1)
            a_t[0] += (EPSILON**i)*n_tt
            a_t[1] += (EPSILON**i)*n_tr

            action_mean[i][j] = a_t[0]

            # Make sure that the action is not lower/greater than -1/1
            if a_t[0] > 1:
                a_t[0] = 1
            if a_t[0] < -1:
                a_t[0] = -1
            if a_t[1] > 1:
                a_t[1] = 1
            if a_t[1] < -1:
                a_t[1] = -1

            logging.debug('Action is : %s', a_t)
            # Do the action
            s_t1, r_t, done, _ = env.step(a_t)

            total_reward += r_t

            # Add to the replay buffer
            buff.add(s_t, a_t, r_t, s_t1, done)

            # Do the batch update
            batch = buff.get_batch(BATCH_SIZE)
            states = np.asarray([e[0] for e in batch])
            actions = np.asarray([e[1] for e in batch])
            rewards = np.asarray([e[2] for e in batch])
            new_states = np.asarray([e[3] for e in batch])
            dones = np.asarray([e[4] for e in batch])
            y_t = np.asarray([e[1] for e in batch])

            # Evaluate the policy
            target_q_values = critic.target_model.predict([new_states, actor.target_model.predict(new_states)])

            for k in range(len(batch)):
                if dones[k]:
                    y_t[k] = rewards[k]
                else:
                    y_t[k] = rewards[k] + GAMMA * target_q_values[k]

            # Update the weights
            loss += critic.model.train_on_batch([states, actions], y_t)
            a_for_grad = actor.model.predict(states)
            grads = critic.gradients(states, a_for_grad)
            actor.train(states, grads)
            actor.target_train()
            critic.target_train()

            # Update state
            s_t = s_t1

            if done:
                break

        # Add the reward
        reward.append(total_reward)

    logging.info('Save the actor model')
    actor.model.save_weights("actormodel.h5", overwrite=True)
    with open("actormodel.json", "w") as outfile:
        json.dump(actor.model.to_json(), outfile)

    logging.info('Save the critic model')
    critic.model.save_weights("criticmodel.h5", overwrite=True)
    with open("criticmodel.json", "w") as outfile:
        json.dump(critic.model.to_json(), outfile)

    # Show the reward
    plt.plot(range(episode_count), reward)
    plt.show()

    mean = np.mean(action_mean, axis=0)
    plt.plot(range(max_steps), mean)
    plt.show()

    # Close the environment
    logging.warning('Close the program')
    env.close()


def play_game():
    """
    The function plays the game 'LunarLander' with continuous actions

    :return: /
    """
    action_dim = 2  # Main Engine / Left-Right engine
    state_dim = 8  # Position, velocity, angular position, angular velocity, ground contact

    episode_count = 10
    max_steps = 1000
    reward = []

    np.random.seed(1337)

    # Start the session of Tensorflow
    sess = tf.Session()
    from keras import backend as K
    K.set_session(sess)

    # Declare Actor/Critic/Replay Buffer
    actor = Actor(sess, state_dim, action_dim, BATCH_SIZE, TAU, LRA)
    critic = Critic(sess, state_dim, action_dim, BATCH_SIZE, TAU, LRC)

    # Import gym environment
    env = gym.make('LunarLanderContinuous-v2')
    env = wrappers.Monitor(env, './tmp/lunarlandar-experiment-1', force=True)

    # Now load the weight
    logging.info("Now we load the weight")
    try:
        actor.model.load_weights("actormodel.h5")
        critic.model.load_weights("criticmodel.h5")
        actor.target_model.load_weights("actormodel.h5")
        critic.target_model.load_weights("criticmodel.h5")
        logging.info("Weight load successfully")
    except:
        logging.warning("Cannot find the weight")

    # Start episode
    for i in range(episode_count):
        logging.info('Episode : %s', i)

        s_t = env.reset()

        done = 0

        total_reward = 0

        # Time steps
        while not done:

            #  Choose an action
            a_t = actor.model.predict(s_t.reshape(1, s_t.shape[0])).reshape(2)

            # Show the image
            env.render()

            # Make sure that the action is not lower/greater than -1/1
            if a_t[0] > 1:
                a_t[0] = 1
            if a_t[0] < -1:
                a_t[0] = -1
            if a_t[1] > 1:
                a_t[1] = 1
            if a_t[1] < -1:
                a_t[1] = -1

            # Do the action
            s_t1, r_t, done, _ = env.step(a_t)

            total_reward += r_t

            # Update state
            s_t = s_t1

            if done:
                logging.info("Done : True")
                break

        # Add the reward
        reward.append(total_reward)

    # Show the reward
    plt.plot(range(episode_count), reward)
    plt.show()

    # Close the environment
    logging.warning('Close the program')
    env.close()

if __name__ == '__main__':
    # Parser
    parser = argparse.ArgumentParser()
    parser.add_argument("type", help="Choice between training [-t] or playing [-p]")
    args = parser.parse_args()

    if args.type is "t":
        train_game()
    elif args.type is "p":
        play_game()
    else:
        print('Choice between training [-t] or playing [-p]')











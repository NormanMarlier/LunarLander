import keras.backend as K
import tensorflow as tf

from keras.layers import Dense, Flatten, Input, concatenate, add, Lambda, Activation
from keras.models import Sequential, Model
from keras.optimizers import Adam



HIDDEN1_UNITS = 60
HIDDEN2_UNITS = 120


class Critic(object):
    def __init__(self, sess, state_size, action_size, batch_size, tau, learning_rate):
        self.sess = sess
        self.BATCH_SIZE = batch_size
        self.TAU = tau
        self.LEARNING_RATE = learning_rate
        self.action_size = action_size

        K.set_session(sess)

        # Now create the model
        self.model, self.action, self.state = self.create_critic_network(state_size, action_size)
        self.target_model, self.target_action, self.target_state = self.create_critic_network(state_size, action_size)
        self.action_grads = tf.gradients(self.model.output, self.action)  # GRADIENTS for policy update
        self.sess.run(tf.global_variables_initializer())

    def gradients(self, states, actions):
        return self.sess.run(self.action_grads, feed_dict={self.state: states, self.action: actions})[0]

    def target_train(self):
        critic_weights = self.model.get_weights()
        critic_target_weights = self.target_model.get_weights()
        for i in range(len(critic_weights)):
            critic_target_weights[i] = self.TAU * critic_weights[i] + (1 - self.TAU) * critic_target_weights[i]
        self.target_model.set_weights(critic_target_weights)

    def create_critic_network(self, state_size, action_dim):
        s = Input(shape=[state_size])
        a = Input(shape=[action_dim], name='action2')
        w1 = Dense(HIDDEN1_UNITS, activation='relu')(s)
        a1 = Dense(HIDDEN2_UNITS, activation='linear')(a)
        h1 = Dense(HIDDEN2_UNITS, activation='linear')(w1)
        h2 = add([h1, a1])
        h3 = Dense(HIDDEN2_UNITS, activation='relu')(h2)
        v = Dense(action_dim, activation='linear')(h3)
        model = Model(input=[s, a], output=v)
        adam = Adam(lr=self.LEARNING_RATE)
        model.compile(loss='mse', optimizer=adam)
        return model, a, s
